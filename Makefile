#
#  Copyright (C) 2020-2022 Bernhard Schelling
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

ifeq ($(ISWIN),)
ISWIN      := $(findstring :,$(firstword $(subst \, ,$(subst /, ,$(abspath .)))))
endif

ifeq ($(ISMAC),)
ISMAC      := $(wildcard /Applications)
endif

PIPETONULL := $(if $(ISWIN),>nul 2>nul,>/dev/null 2>/dev/null)

SOURCES := \
  *.cpp       \
  src/*.cpp   \
  src/*/*.cpp \
  src/*/*/*.cpp

CPUFLAGS := $(MAKE_CPUFLAGS)
STRIPCMD := strip --strip-all
ifneq ($(ISWIN),)
  OUTNAME := dosbox_pure_libretro.dll
  CXX     ?= g++
  LDFLAGS := -Wl,--gc-sections -fno-ident
  COMMONFLAGS += -pthread
else ifeq ($(platform),windows) # For MSYS2 only
  OUTNAME := dosbox_pure_libretro.dll
  CXX     ?= g++
  LDFLAGS := -Wl,--gc-sections -fno-ident
else
  OUTNAME := dosbox_pure_libretro.so
  CXX     ?= g++
  LDFLAGS := -Wl,--gc-sections -fno-ident
  COMMONFLAGS += -pthread
  ifeq ($(CPUFLAGS),)
    # ARM optimizations
    PROCCPU := $(shell cat /proc/cpuinfo))
    ifneq ($(and $(filter ARMv7,$(PROCCPU)),$(filter neon,$(PROCCPU))),)
      CPUFLAGS := -marm -mcpu=cortex-a72 -mfpu=neon-fp-armv8 -mfloat-abi=hard -ffast-math
    else ifeq ($(ARM_RPI4), 1)
      CPUFLAGS := -marm -mcpu=cortex-a72 -mfpu=neon-fp-armv8 -mfloat-abi=hard -ffast-math
    else
      ifeq ($(CORTEX_A7), 1)
        CPUFLAGS += -marm -mcpu=cortex-a7
        ifeq ($(ARM_NEON), 1)
          CPUFLAGS += -mfpu=neon-vfpv4
        endif
      endif
      ifeq ($(ARM_HARDFLOAT), 1)
        CPUFLAGS += -mfloat-abi=hard
      endif
    endif
  endif
endif

ifeq ($(BUILD),DEBUG)
  BUILDDIR := debug
  CXXFLAGS := -O0 -g -Wall -Wextra $(COMMONFLAGS) $(CPUFLAGS)
else
  BUILDDIR := release
  CXXFLAGS := -O3 -DNDEBUG $(COMMONFLAGS) $(CPUFLAGS)
endif

OBJDIR := $(BUILDDIR)/obj

OBJS := $(addprefix $(OBJDIR)/,$(patsubst %.cpp,%.o,$(SOURCES)))

.DEFAULT_GOAL := $(BUILDDIR)/$(OUTNAME)

$(OBJDIR)/%.o: %.cpp
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILDDIR)/$(OUTNAME): $(OBJS)
	$(CXX) -o $@ $^ $(LDFLAGS)
	$(STRIPCMD) $@

clean:
	rm -rf $(BUILDDIR)
